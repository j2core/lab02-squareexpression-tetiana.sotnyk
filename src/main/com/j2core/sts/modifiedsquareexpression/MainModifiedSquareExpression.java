package com.j2core.sts.modifiedsquareexpression;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * This project is calculating result for square expression with entry value variable from console
 */
public class MainModifiedSquareExpression {

    static final double INACCURACY = 0.0000001;

    public static void main(String[] args) {

        double a, b, c, d, x1, x2;

        Scanner scanner = new Scanner(System.in);

        System.out.println("You want to solve a square expression");

        try {
            System.out.println("  Enter value number 'a' ");
            a = scanner.nextDouble();
            System.out.println("  Enter value number 'b' ");
            b = scanner.nextDouble();
            System.out.println("  Enter value number 'c' ");
            c = scanner.nextDouble();

            if (a == 0.0) {
                System.out.println("  Expression " + a + "x^2 + " + b + "x + " + c + " = 0   -  it's not quadratic equation");
                return;
            }
            if (b == 0.0 && c == 0.0) {
                System.out.println("  Expression " + a + "x^2 + " + b + "x + " + c + " = 0   -  it's incomplete quadratic equation has one solution  x = 0");
                return;
            }

            d = b * b - 4 * a * c;
            if (d < -INACCURACY) {
                System.out.println("  Expression " + a + "x^2 + " + b + "x + " + c + " = 0   -  there are no real roots");
                return;
            }
            if (Math.abs(d) <= INACCURACY) {
                x1 = -b / (2 * a);
                System.out.println("  Expression " + a + "x^2 + " + b + "x + " + c + " = 0   -  has one solution  x = " + x1);
            } else {
                x1 = (-b + Math.sqrt(d)) / (2 * a);
                x2 = (-b - Math.sqrt(d)) / (2 * a);
                System.out.println("  Expression " + a + "x^2 + " + b + "x + " + c + " = 0   -  has two solution");
                System.out.println("       x1 = " + x1 + "      x2 = " + x2);
            }

        } catch (InputMismatchException ex) {
            System.out.println("Entered characters are not valid");
        }
    }
}
